﻿using UnityEngine;
using System.Collections;

/*
 * PlayerTricks
 * 
 * This scripts is responcible for everything associated
 * with performing tricks as well as completing jumps.
 * 
 * This script should be attached to the renderable player object
 * as it handles the physical manipulation of the object while
 * performing the tricks.
 * 
 */
public class PlayerTricks : MonoBehaviour {
	
	public bool tricksEnabled = false;				// Allows the player to perfrom tricks
	public float scoreMultiplier;					// A multiplier that is applied to score for successful tricks
	private float airTimeScoreMultiplier = 20.0f;	// A multiplier for how much points are given for air time
	bool verticleRotated, horizontalRotated;		// Remembers in the player has rotated 180 degrees in a direction
	bool rotatingVerticle, rotatingHorizontal;		// Cap a single rotation axis based on the users intent
	public float score;								// The score is that is given for completing a jump/trick.
	bool holdTricks = false;

	// Make private later on
	private float rotationSpeed = 360;						// How fast the player rotates
	
	// Reference to other scripts
	FuelManager fuelManager;
	ScoreManager scoreManager;
	GameManager	gameManager;
	PlayerMain player;

	/*
	 * The initialization function.
	 * Finds reference to the private scripts and initialize
	 * the rotation speed.
	 */
	void Start()
	{
		fuelManager = GameObject.Find("GameManager").GetComponent<FuelManager>();
		scoreManager = GameObject.Find("GameManager").GetComponent<ScoreManager>();
		player = GameObject.Find("Player").GetComponent<PlayerMain>();

		if (!fuelManager)
			Debug.Log("PlayerTricks cannot locate the FuelManager");
		if (!scoreManager)
			Debug.Log("PlayerTricks cannot locate the scoreManager");
		if (!scoreManager)
			Debug.Log("PlayerTricks cannot locate the PlayerMain");

		// Load in Vehicles rotation speed
		if(PlayerPrefs.GetFloat("Tricks")!= 0.0f)
		{
			Debug.Log ("Tricks adjusted");
			rotationSpeed = rotationSpeed * PlayerPrefs.GetFloat("Tricks");
		}
	}

	/*
	 * Called every frame.
	 * Used to check if the player can perform tricks
	 */
	void Update()
	{
		if(tricksEnabled)
		{
			PerformTrick();
		}
	}
	
	/*
	 * Is used to check when the player hits something while performing a trick.
	 * I.e. if they fail/complete the trick.
	 */
	void OnTriggerEnter(Collider collider)
	{
		if(tricksEnabled && collider.name != "Cube" && collider.name != "Collectable" && collider.name != "Powerup" && collider.tag != "Rocket")
		{
			if(!holdTricks)
			{
				tricksEnabled = false;
				if(CheckRotation())
				{
					// Completed the jump successfully now reset rotation
					transform.localRotation = Quaternion.Euler(Vector3.zero);

					Debug.Log("Trick successfully performed!");
					Debug.Log("You hit " + collider.name);
					score = score * scoreMultiplier;
					fuelManager.GainFuel(0.15f * scoreMultiplier);
					scoreManager.IncreaseScore(score);


				}
				else
				{
					Debug.Log("Trick Failed!");
					player.KillPlayer("Trick");
					GameObject.Find("GameManager").GetComponent<GameManager>().GameOver("Trick");
				}
			}
		}
	}

	/*
	 * Checks to see if the player is within a decent
	 * rotation when trying to land after a jump
	 */
	bool CheckRotation()
	{
		// rotation accuracy
		int rotAcc = 30;

		// Test against all rotation angles
		if((transform.localRotation.eulerAngles.z > 360 - rotAcc ||
		   transform.localRotation.eulerAngles.z  < rotAcc) &&
		   (transform.localRotation.eulerAngles.x > 360 - rotAcc ||
		   transform.localRotation.eulerAngles.x  < rotAcc) &&
		   (transform.localRotation.eulerAngles.y > 360 - rotAcc ||
		   transform.localRotation.eulerAngles.y < rotAcc))
		{	
			return true;
		}
		return false;

	}

	/*
	 * Makes the user perform a trick.
	 * This can only ever be called once the user has hit
	 * a jump and is in the air.
	 */
	void PerformTrick()
	{
		// How much error we account for. Changing this value
		// will make it more/less persise when performing tricks.
		int rotAcc = 10;


		// Get the keyboard input to create the rotation
		if(!rotatingHorizontal)
			{
			if(Input.GetKey(KeyCode.W))
			{
				transform.Rotate(Vector3.left * Time.deltaTime * rotationSpeed);
			}
			if(Input.GetKey(KeyCode.S))
			{
				transform.Rotate(Vector3.right * Time.deltaTime * rotationSpeed);
			}

			// Check to see if the player is meant to be rotating in the z direction
			if(((transform.localRotation.eulerAngles.x < (360 - rotAcc/2) && transform.localRotation.eulerAngles.x > rotAcc/2))
			   ||(transform.localRotation.eulerAngles.y > 175 && transform.localRotation.eulerAngles.y < 185))
			{
				rotatingVerticle = true;
			}
			else 
				rotatingVerticle = false;
		}
		if(!rotatingVerticle)
		{
			if(Input.GetKey(KeyCode.A))
			{
				transform.Rotate(Vector3.forward * Time.deltaTime * rotationSpeed);
			}
			if(Input.GetKey(KeyCode.D))
			{
				transform.Rotate(Vector3.back * Time.deltaTime * rotationSpeed);
			}
			
			// Check to see if the player is meant to be rotating in the z direction
			if( transform.localRotation.eulerAngles.z < (360 - rotAcc/2) && transform.localRotation.eulerAngles.z > rotAcc/2
			   )
			{
				rotatingHorizontal = true;
			}
			else
				rotatingHorizontal = false;
		}

		// Check to see if the player has rotated 180 degrees
		if(transform.localRotation.eulerAngles.z > (180 - rotAcc) && transform.localRotation.eulerAngles.z < (180 + rotAcc) && rotatingHorizontal)
		{
			horizontalRotated = true;
		}
		if(rotatingVerticle && (transform.localRotation.eulerAngles.x > (360 - rotAcc) || transform.localRotation.eulerAngles.x < rotAcc)
		   && (transform.localRotation.eulerAngles.y > 175 && transform.localRotation.eulerAngles.y < 185) )
		{
			verticleRotated = true;
		}


		// Check for a full rotation
		if(horizontalRotated && (transform.localRotation.eulerAngles.z > (360 - rotAcc) || transform.localRotation.eulerAngles.z < rotAcc))
		{
			horizontalRotated = false;
			scoreMultiplier++;
			Debug.Log(" You have performed a trick ");
		}
		if(verticleRotated && (transform.localRotation.eulerAngles.x > (360 - rotAcc) || transform.localRotation.eulerAngles.x < rotAcc)
		   && (transform.localRotation.eulerAngles.y > 355 || transform.localRotation.eulerAngles.y < 5))
		{
			verticleRotated = false;
			scoreMultiplier++;
			Debug.Log(" You have performed a trick ");
		}



	}
	
	/*
	 * Enables the player to perform tricks,
	 * i.e. the player has hit a jump and is in the air.
	 */
	public void EnableTricks()
	{
		score = 0;
		Debug.Log("Enabling tricks");
		tricksEnabled = true;
		scoreMultiplier = 1;
		StartCoroutine(AirTime());
		StopCoroutine(HoldTricks());
		StartCoroutine(HoldTricks());
	}

	/*
	 * Continuesly adds score while the player is in the air.
	 */
	IEnumerator AirTime()
	{
		while(tricksEnabled)
		{
			score += Time.deltaTime * airTimeScoreMultiplier;
			yield return null;
		}
	}	


	IEnumerator HoldTricks()
	{
		holdTricks = true;
		yield return new WaitForSeconds (0.5f);
		holdTricks = false;

	}	


	/*
	 * Gives the player feedback on whether they
	 * can successfully land a trick or not.
	 */
	void OnGUI()
	{
		if(tricksEnabled)
		{
			GUI.Label (new Rect((Screen.width / 2)-25,(Screen.height - 150),400,100),Globals.printString("x" + scoreMultiplier.ToString() + "!", 30));
		}
	}

}
