using UnityEngine;
using System.Collections;

/* PlayerMain
 * 
 */
public class PlayerMain : MonoBehaviour {
	

	private float movementSpeed = 100;				// Base movement speed
	private float rotationSpeed = 100;				// Base roation speed

	// Check for specific conditions
	private bool collided = false;
	private bool outOfFuel = false;
	private bool trickFailed = false;
	private bool playerAlive = true;

	// Reference to other scripts
	private GameObject player;
	private GameObject playerModel;
	private Rigidbody playerRigidBody;
	private PlayerTricks playerTricks;
	private FuelManager fuelManager;
	
	/*
	 * Check to make our player has the all the necessary components
	 * To function properly and set up variables
	 */
	private void Init() {
		player = this.gameObject;
		playerRigidBody = player.rigidbody;
		playerTricks = gameObject.GetComponentInChildren<PlayerTricks>();
		fuelManager = GameObject.Find ("GameManager").GetComponent<FuelManager>();
		playerModel = GameObject.FindWithTag("PlayerModel");

		if(!playerRigidBody)
			Debug.Log("player object has no rigidbody attached");
		if(!playerTricks)
			Debug.Log("player object has no PlayerTricks script attached");

		// Load in the Vehicle upgrades
		if(PlayerPrefs.GetFloat("Handling") != 0.0f)
		{
			Debug.Log("Handling adjusted");
			rotationSpeed = rotationSpeed * PlayerPrefs.GetFloat("Handling");
		}
		if(PlayerPrefs.GetFloat("Speed") != 0.0f)
		{
			Debug.Log("Speed adjusted");
			movementSpeed = movementSpeed * PlayerPrefs.GetFloat("Speed");
		}
	
	}

	/*
	 * Will call the Init() function.
	 */
	void Start () {
		Init();
	}

	/*
	 * Update function that will make the player move and rotate
	 */
	void Update () {
		RotatePlayer();
		MovePlayer();
	}

	/*
	 * Will check if the player is currently alive
	 */
	public bool IsDead() {

		if(!playerAlive)
		{
			// Check fuel amount
			if (fuelManager.GetFuel() < 0)
				outOfFuel = true;

			if (outOfFuel == true || collided == true)
				return true;
			return true;
		}
		return false;
	}

	/*
	 * Initiates the gameover state for a given reason
	 */
	public void KillPlayer(string reason)
	{
		switch(reason)
		{
		case "Trick":
			trickFailed = true;
			break;
		case "Collision":
			collided = true;
			break;
		}
		Debug.Log ("Kill called");

		GameObject.Find("GameManager").GetComponent<GameManager>().GameOver("Collision");
	}

	/*
	 * Handles the gameover state for the player
	 */
	public void GameOver()
	{
		playerAlive = false;

		movementSpeed = 0.0f;

		Destroy(GameObject.FindWithTag("PlayerModel"));

	}

	/*
	 * Moves the player forward
	 */
	void MovePlayer()
	{

		// Set the velocity for the x and z axis
		Vector3 velocity = transform.forward * movementSpeed;

		// Keep the previous y velocity
		velocity.y = rigidbody.velocity.y;

		// Change the velocity of the object
		rigidbody.velocity = velocity;
	}
	
	/*
	 * Rotates the player based on where the mouse is horizontally
	 * with respets to the player
	 */
	void RotatePlayer()
	{
		float screenWidth = Screen.width * 0.5f;
		float mousePlacement = Input.mousePosition.x;
		float inputRotation = mousePlacement - screenWidth;

		// Make rotation between -1 and 1
		inputRotation = inputRotation/screenWidth;

		transform.RotateAround(transform.position,Vector3.up, inputRotation*rotationSpeed* Time.deltaTime);
	}

	/*
	 * Handles collision that will kill the player 
	 */
	void OnCollisionEnter(Collision collision) {

		switch (collision.gameObject.tag) {
			case "Wall":
			case "DestructibleObject":

				// Try and destroy the object
				DestructableObject ds;
				ds = collision.gameObject.GetComponent<DestructableObject>();
				if(ds)
				{
					ds.KillObject();
				}

				KillPlayer ("Collision");
				break;
		}
	}
}
