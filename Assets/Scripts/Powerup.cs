﻿using UnityEngine;
using System.Collections;

public class Powerup : MonoBehaviour {

	void Update () {
		transform.Rotate (0, 3 * Time.deltaTime, 0);
	}


	void OnTriggerEnter(Collider collision)
	{
		if(collision.gameObject.name == "Player")
		{
			GameObject.Find("GameManager").GetComponent<PowerupManager>().ReceivePowerup();
			Destroy(gameObject);
		}
	}
}
