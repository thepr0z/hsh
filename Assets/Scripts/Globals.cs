
using UnityEngine;
using System.Collections;

/*
 * Globals
 * 
 * This script hold global functions
 */
public class Globals : MonoBehaviour {

	/* Call me for any string */
	public static string printString(string str, int fontSize) {

		return "<size=" + fontSize + ">" + str + "</size>";

	}

	// print a label to screen with specified location
	public static void labelPrint(string str, int fontSize, Rect location) {

		GUILayout.BeginArea (location);
		GUILayout.Label (printString(str,fontSize));
		GUILayout.EndArea ();
	}

	// print a label to screen without location (e.g. it's already supposed to be in a BeginArea()
	public static void labelPrint(string str, int fontSize) {

		GUILayout.Label (printString(str,fontSize));
	}



}
