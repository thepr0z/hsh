﻿using UnityEngine;
using System.Collections;


/* Fuel Manager
 * 
 * This scripts is responcible for everything related
 * the players fuel.
 */
public class FuelManager : MonoBehaviour {

	public float fuelDecreaseRate;			// How fast the fuel will decrease
	float fuelAmount = 1.0f;			// The current amount of fuel the player has
	public Texture tex;						// The texture that will be applied to the fuel bar
	private bool gainedFuel = false;
	private int prevAmount;

	// Update is called once per frame
	void Update () {
		ManageFuel();
	}

	/*
	 * Decrements the fuel to simulate the use of it.
	 * Also will call game over when fuel runs out.
	 */
	void ManageFuel()
	{
		fuelAmount -= fuelDecreaseRate * Time.deltaTime;
		if(fuelAmount <= 0.0f)
			GameObject.Find("GameManager").GetComponent<GameManager>().GameOver("Fuel");
	}

	IEnumerator fuelDisplay() {

		gainedFuel = true;
		yield return new WaitForSeconds (3);
		gainedFuel = false;
		}

	/*
	 * Getter for fuel.
	 */
	public float GetFuel() {
		return fuelAmount;
	}

	/*
	 * GUI function for displaying the fuel gauge on the screen.
	 */
	void OnGUI() {
		float maxHeight = (float)((Screen.height)*0.5);
		GUILayout.BeginArea(new Rect(30,30,Screen.width,Screen.height));

		Globals.labelPrint ("Fuel", 20);
		GUILayout.Box(tex,GUILayout.Height(maxHeight*fuelAmount),GUILayout.Width(40));

		GUILayout.EndArea ();

		if(gainedFuel)
			Globals.labelPrint("You gained " + prevAmount.ToString () + " fuel", 25,new Rect(Screen.width/2-50,Screen.height/2,Screen.width,Screen.height));

	}

	/*
	 * Public fuel incrementer.
	 */
	public void GainFuel( float amount )
	{
		fuelAmount += amount;
		prevAmount = (int)(amount* 100);
		StopCoroutine (fuelDisplay ());
		StartCoroutine (fuelDisplay ());

		if (fuelAmount > 1.5f)
						fuelAmount = 1.5f;
	}

	/*
	 * Game over state.
	 * Removes the decrease amount so that it doesnt keep
	 * decremeting when dead.
	 */
	public void GameOver()
	{
		fuelDecreaseRate = 0.0f;
	}
}
