﻿using UnityEngine;
using System.Collections;

/*
 * PowerupManager
 * 
 * This scripts is handles the execution of powerups.
 */
public class PowerupManager : MonoBehaviour {

	// Used to save if a powerup is currently active
	public bool multishotActive, unlimitedRocketsActive, slowMotionActive;

	// Power up stats
	float slowMotionDuration = 10.0f;				// The duration for slow motion
	float slowMotionAmount = 0.5f;					// What the slow motion time scale will be set to
	float unlimitedRocketDuration = 5.0f;			// The duration or unlimted fire
	float multishotDuration = 10.0f;				// The duration for multi shot fire

	// Caches how long a powerup has been running for
	public float activeTimeMulti = 0.0f;
	public float activeTimeSlow = 0.0f;
	public float activeTimeUnlim = 0.0f;
	
	public Texture tex;								// The texture to be applied the the background of the powerup GUI
	public bool gameOver = false;					// Will remove GUI if gameover has been called
	public int fontSize = 25;						// The font size for the GUI texts
	
	RocketManager rocketManager;					// Reference to the RocketManager
	
	/*
	 * Used to initialize private variables
	 */
	void Start () {
		rocketManager = gameObject.GetComponent<RocketManager>();
	}
	
	/*
	 * Gives the player a random non-active powerup.
	 */
	public void ReceivePowerup()
	{
		// Check if there is a powerup that is not active
		if(!multishotActive || !unlimitedRocketsActive || !slowMotionActive)
		{
			// Try a random powerup
			switch(Random.Range(1,4))
			{
			case 1:
				if(!slowMotionActive)
				{
					StartCoroutine(SlowMotion());
					return;
				}
				break;
			case 2:
				if (!unlimitedRocketsActive)
				{
					rocketManager.StartUnlimtedRockets(unlimitedRocketDuration);
					StartCoroutine(UnlimitedRocketsActiveDuration());
					return;
				}
				break;
			case 3:
				if(!multishotActive)
				{
					rocketManager.StartMultishot(multishotDuration);
					StartCoroutine(MultishotActiveDuration());
					return;
				}
				break;
			}

			// If the powerup selected was already active, try again.
			ReceivePowerup();
		}
	}
	
	/*
	 * Puts the game into slow motion
	 */
	IEnumerator SlowMotion()
	{
		slowMotionActive = true;

		// Slow the game down
		Time.timeScale = slowMotionAmount;

		// Do nothing until the duration has ended
		while (activeTimeSlow < slowMotionDuration*slowMotionAmount) {
			activeTimeSlow += Time.deltaTime;
			yield return null;
		}

		// Set the game back to normal speed
		Time.timeScale = 1.0f;
		activeTimeSlow = 0.0f;
		slowMotionActive = false;
	}

	/*
	 * A timer function that keeps track of how long
	 * the unlimited rockets powerup has been active for
	 */
	IEnumerator UnlimitedRocketsActiveDuration()
	{
		unlimitedRocketsActive = true;

		// Do nothing until the duration has ended
		while (activeTimeUnlim < unlimitedRocketDuration) {
			activeTimeUnlim += Time.deltaTime;
			yield return null;
		}

		// Reset values
		activeTimeUnlim = 0.0f;
		unlimitedRocketsActive = false;
	}

	/*
	 * A timer function that keeps track of how long
	 * the multishot powerup has been active for
	 */
	IEnumerator MultishotActiveDuration()
	{
		multishotActive = true;

		// Do nothing until the duration has ended
		while (activeTimeMulti < multishotDuration) {
			activeTimeMulti += Time.deltaTime;
			yield return null;
		}

		// Reset values
		activeTimeMulti = 0.0f;
		multishotActive = false;
	}

	/*
	 * Game over state
	 */
	public void GameOver() {

		gameOver = true;
		}

	/*
	 * GUI function for powerups.
	 * This will display a bar to show the user how long they have
	 * before a powerup becomes in active as well as what powerups
	 * are currently active.
	 */
	void OnGUI() {

		float barSize = 200.0f;

		if (!gameOver) {

			// Duration bar for multishot powerup
			if (multishotActive) {
				GUILayout.BeginArea (new Rect (Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2)); 
				GUILayout.Label (Globals.printString("Multishot Active!",fontSize));
				GUILayout.Box (tex, GUILayout.Height (20), GUILayout.Width ((multishotDuration - activeTimeMulti) * barSize));
				GUILayout.EndArea ();
			}

			// Duration bar for slowmotion powerup
			if (slowMotionActive) {
				GUILayout.BeginArea (new Rect (Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2)); 
				GUILayout.Label (Globals.printString("Slow Motion Active!",fontSize));
				GUILayout.Box (tex, GUILayout.Height (20), GUILayout.Width ((slowMotionDuration * slowMotionAmount - activeTimeSlow) * barSize));
				GUILayout.EndArea ();
			}

			// Duration bar for unlimited rockets powerup
			if (unlimitedRocketsActive) {
				GUILayout.BeginArea (new Rect (Screen.width / 4, Screen.height / 4, Screen.width / 2, Screen.height / 2)); 
				GUILayout.Label(Globals.printString("Unlimited Rockets Active!",fontSize));
				GUILayout.Box (tex, GUILayout.Height (20), GUILayout.Width ((unlimitedRocketDuration - activeTimeUnlim) * barSize));
				GUILayout.EndArea ();
			}

		}
	}
}
