﻿using UnityEngine;
using System.Collections;


/* RocketManager.
 * This script handles the creation of rockets and rocket related powerups.
 */
public class RocketManager : MonoBehaviour {


	private GameObject playerModel;		// The GameObject which to spawn the rocket
	private bool rocketReady;			// A check to see if a rocket can fire
	public GameObject rocket;			// The rocket object to instantiate
	public float reloadTime;			// The time it takes between shots
	public Texture2D rocketTex;			// Texture for gui representation of rocket
	bool multishotActive;				// Check if the player has the multishot powerup
	
	/*
	 * Initializes all variables.
	 */
	void Start () {
	
		playerModel = GameObject.FindGameObjectWithTag("PlayerModel");
		rocketReady = true;

		if(!playerModel)
			Debug.Log("RocketManager cannot find the playerModel");
		if(!rocket)
			Debug.Log("RocketManager has no rocket attached");

	}

	/*
	 * The public function for starting the multishot functionality.
	 */
	public void StartMultishot(float duration)
	{
		StartCoroutine(ActiveMutishot(duration));
	}

	/*
	 * Sets the multishot functionality to true for the duration.
	 */
	IEnumerator ActiveMutishot(float duration)
	{
		multishotActive = true;
		yield return new WaitForSeconds(duration);
		multishotActive = false;
	}

	/*
	 * The public function for starting the unlimited rockets functionality.
	 */
	public void StartUnlimtedRockets(float duration)
	{
		StartCoroutine(ActiveUnlimtedRockets(duration));
	}

	/*
	 * Activates unlimited rockets by remvoing the reloadTime.
	 */
	IEnumerator ActiveUnlimtedRockets(float duration)
	{
		// Stop the current reload time
		StopCoroutine(ReloadRocket());
		// Cache the old time
		float oldReloadTime = reloadTime;
		// Remove the reload time
		reloadTime = 0.0f;

		rocketReady = true;
		yield return new WaitForSeconds(duration);

		// Reset values
		reloadTime = oldReloadTime;
	}

	/*
	 * Is only used to check if the player wants to 
	 * fire a rocket.
	 */
	void Update () {
		if(Input.GetMouseButtonDown(0))
			FireRocket();
	}
	
	/*
	 * Handles the firing of rockets
	 */
	void FireRocket()
	{
		if(rocketReady)
		{
			if(multishotActive)
			{
				// Create 2 extra rockets with a rotational offset
				Instantiate(rocket, playerModel.transform.position, playerModel.transform.rotation * Quaternion.Euler(Vector3.up * 10.0f));
				Instantiate(rocket, playerModel.transform.position, playerModel.transform.rotation * Quaternion.Euler(Vector3.down * 10.0f));
			}
			Instantiate(rocket, playerModel.transform.position, playerModel.transform.rotation);

			// Reload the rocket
			StartCoroutine(ReloadRocket());
		}
	}
	
	/*
	 * A timer to activate the rocket
	 */
	IEnumerator ReloadRocket() {
		rocketReady = false;
		yield return new WaitForSeconds(reloadTime);
		rocketReady = true;
	}

	/*
	 * GUI function to display when a rocket is ready
	 */
	void OnGUI() {

		if(rocketReady)
			GUI.Label (new Rect((Screen.width / 2),20,100,100),rocketTex);
		}

	}
