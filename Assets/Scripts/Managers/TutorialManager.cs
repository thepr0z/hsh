using UnityEngine;
using System.Collections;

/* 
 * TutorialManager
 * 
 * This script will display times tutorial messages to the user
 */
public class TutorialManager : MonoBehaviour {

	bool gameOver = false;			// Cache if gameover has been reached

	/*
	 * Game over state
	 */
	public void GameOver() {
		gameOver = true;
	}

	/*
	 * GUI function that will display the messages
	 */
	void OnGUI() {
		
		if (gameOver == false) {
			//set up tutorial message area

			if (Time.time > 0.0f && Time.time < 2.0f)
				Globals.labelPrint("Move the mouse left and right to steer the car!",25,new Rect(Screen.width/2,Screen.height/3,400,400));

			if (Time.time > 3.0f && Time.time < 7.0f)
				Globals.labelPrint("Perform a front flip by holding 'W' while in the air!",25,new Rect(Screen.width/2,Screen.height/3,400,400));

			if (Time.time > 8.0f && Time.time < 10.0f)
				Globals.labelPrint("Click the left mouse button to fire a rocket!",25,new Rect(Screen.width/2,Screen.height/3,400,400));

			if (Time.time > 11.0f && Time.time < 13.0f)
				Globals.labelPrint("Collect all the glowing items to progress",25,new Rect(Screen.width/2,Screen.height/3,400,400));

			if (Time.time > 14.0f && Time.time < 16.0f)
				Globals.labelPrint("But watch out for obstacles!",25,new Rect(Screen.width/2,Screen.height/3,400,400));
			

		}
	}

}

