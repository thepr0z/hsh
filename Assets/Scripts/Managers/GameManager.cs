using UnityEngine;
using System.Collections;

/*
 * GameManager
 * 
 * This script is repsoncible for running the level and tieing all
 * the componets together. 
 */
public class GameManager : MonoBehaviour {

	// Reference to other scripts
	PlayerMain player;
	ScoreManager scoreManager;
	FuelManager fuelManager;
	PowerupManager powerupManager;
	TutorialManager tutorialManager;

	// Current game states
	bool gameStart = true;
	bool gameOver = false;
	bool inTutorial = true;
		
	string deathReason;					// Used to hold the reason why the player died.
	private int collectablesLeft = 3;	// The number of collectables currently in the level.
										// When reaches 0 the player has won the game.
	public int fontSize = 30;			// The font size for GUI calls.

	/*
	 * Initializes private variables and pauses the game.
	 */
	void Start() {

		// Set up the private variables
		player = GameObject.Find("Player").GetComponent<PlayerMain> ();
		scoreManager = gameObject.GetComponent<ScoreManager>();
		fuelManager = gameObject.GetComponent<FuelManager>();
		tutorialManager = gameObject.GetComponent<TutorialManager> ();
		powerupManager = gameObject.GetComponent<PowerupManager> ();

		// Pause the game
		Time.timeScale = 0.0f;
	}

	/*
	 * Called when a collectable has been gathered by the player.
	 * Decrements the current collectable amount.
	 */
	public void Collect() {
		if (collectablesLeft > 0) 		
			collectablesLeft -= 1;
	}

	/*
	 * A check if the player has gathered all the collectables.
	 */
	bool HasAllCollectables() {
		if (collectablesLeft == 0)
			return true;
		return false;
	}

	/*
	 * The main HUD.
	 * Displays	- Collectables
	 * 			- Game over menu
	 * 			- Start game button
	 */
	void OnGUI() {

		Rect area = new Rect ((Screen.width - 100), 15, 100, 100);
		Rect textArea = new Rect ((Screen.width - 170), 60, 200, 100);

		// Collectable display
		Globals.labelPrint (collectablesLeft.ToString () + "/3", fontSize,area);;
		Globals.labelPrint ("Collectables Remaining",15, textArea);

		// The initial button to start the game
		if(gameStart)
		{
			if (GUI.Button(new Rect(Screen.width/4, Screen.height/4, Screen.width/2, Screen.height/4),Globals.printString ("Start Game",fontSize)))
			{
				Time.timeScale = 1.0f;
				gameStart = false;
			}
		}

		// Lets the layer know that they hove completed the level 
		if(!gameOver)
		{
			
			if(player.IsDead () == false && HasAllCollectables () == true) {
				Globals.labelPrint("You have won!",fontSize,new Rect((Screen.width/2),Screen.height/2,500,500));	
			}
		}

		// The gameover menu screen
		if(gameOver)
		{
			GUILayout.BeginArea(new Rect(Screen.width/4, Screen.height/4,Screen.width/2, Screen.height/2)); 

			GUILayout.Label("You have died because you " + deathReason);
			GUILayout.Label("High Score: " + PlayerPrefs.GetInt("HighScore"));

			if(GUILayout.Button("Play Again"))
			{
				Application.LoadLevel("Level1");
			}
			if(GUILayout.Button("Main Menu"))
			{
				Application.LoadLevel("MainMenu");
			}
			GUILayout.EndArea();
		}
		
	}

	/*
	 * Master Game Over state.
	 * When ever the player reaches the game over state, this method is called.
	 * It delegates out to all nessecary scripts that Game Over has been reached.
	 * It also caches the reason so that the player has some sort of feedback.
	 */
	public void GameOver(string reason)
	{
		gameOver = true;

		// Cache the reason why game over was called.
		switch (reason)
		{
		case "Trick": deathReason = "failed to land a trick"; break;
		case "Fuel": deathReason = "ran out of fuel"; break;
		case "Collision": deathReason = "hit a wall"; break;
		}
	
		// Call gameover states
		scoreManager.GameOver();
		fuelManager.GameOver();
		tutorialManager.GameOver ();
		player.GameOver();
		powerupManager.GameOver ();
	}

}
