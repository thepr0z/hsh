﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	PlayerMain player;
	PowerupManager powerupManager;
	public AudioSource backgroundMusic;

	// Use this for initialization
	void Start () {

		player = GameObject.Find("Player").GetComponent<PlayerMain> ();
		powerupManager = gameObject.GetComponent<PowerupManager> ();
		AudioListener.pause = false;
	}
	
	// Update is called once per frame
	void Update () {


		//pause the music if the player dies
		if(player.IsDead()){
			AudioListener.pause = true;
		}

		if (powerupManager.slowMotionActive) {
						backgroundMusic.pitch = 0.5f;
				} else {
			backgroundMusic.pitch = 1.0f;		
		}

		}
	}
