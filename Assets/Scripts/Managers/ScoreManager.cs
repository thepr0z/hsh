using UnityEngine;
using System.Collections;

/*
 * ScoreManager
 * 
 * This script will handle everything related to the score
 */
public class ScoreManager : MonoBehaviour {

	public float scoreIncrement;			// Amount the score will increase for surviving		
	float score = 0.0f;						// Current score the user has				
	public int fontSize = 30;				// Font size for the GUI calls

	/*
	 * Will increase the score continuesly
	 */
	void Update () {
		score +=  scoreIncrement * Time.deltaTime;
	}

	/*
	 * GUI function for displaying the score
	 */
	void OnGUI() 
	{
		int iScore = (int)score;
		Globals.labelPrint ("Score", 20, new Rect (85, 30, Screen.width, Screen.height));
		Globals.labelPrint (iScore.ToString (), fontSize, new Rect (90, 50, Screen.width, Screen.height));
	}

	/*
	 * Incrementer for score
	 */
	public void IncreaseScore(float inc) {
		score += inc;
	}

	/*
	 * Getter for score
	 */
	public float GetScore()
	{
		return score;
	}

	/*
	 * Called when the gameover state has been reached.
	 * Saves the score in the high score list
	 * and stops the score from incrementing.
	 */
	public void GameOver()
	{
		scoreIncrement = 0.0f;

		int newScore = (int)score;
		if(newScore > PlayerPrefs.GetInt("HighScore"))
		{
			PlayerPrefs.SetInt("HighScore", newScore);
		}

	}
}
