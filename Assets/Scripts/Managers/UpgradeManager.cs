﻿using UnityEngine;
using System.Collections;

/*
 * UpgradeManager
 * 
 * This scripts handles applying all the upgrades to
 * the user vehicle through PlayerPrefs.
 * 
 * Not going to comment as it is just a quick dummy
 * to give upgrade functionality
 */
public class UpgradeManager : MonoBehaviour {

	public GUIText tyreSelection, bodySelection;
	public GUIText speedText, handlingText, tricksText;
	
	public enum Body{Body1 , Body2, Body3};
	public enum Tyres{Tyre1, Tyre2, Tyre3};

	Body body;
	Tyres tyre;

	float speed, handling, tricks;
	
	// Use this for initialization
	void Start () {
		speed = handling = tricks = 0.0f;
		body = Body.Body1;
		tyre = Tyres.Tyre1;

		SetBodyStats();
	}
	
	// Update is called once per frame
	void Update () {
		mouseInput();
	}

	/*
	 * Changes the body selection to the next body
	 */
	public void NextBody()
	{
		switch(body)
		{
		case Body.Body1:
			body = Body.Body2;
			break;
		case Body.Body2:
			body = Body.Body3;
			break;
		case Body.Body3:
			body = Body.Body1;
			break;
		}
	SetBodyStats();
	}

	public void NextTyre()
	{
		switch(tyre)
		{
		case Tyres.Tyre1:
			tyre = Tyres.Tyre2;
			break;
		case Tyres.Tyre2:
			tyre = Tyres.Tyre3;
			break;
		case Tyres.Tyre3:
			tyre = Tyres.Tyre1;
			break;
		}
		SetBodyStats();
	}

	void SetBodyStats()
	{
		float tspeed, thandling, ttricks;
		tspeed = thandling = ttricks = 0.0f;

		switch(body)
		{
		case Body.Body1:
			tspeed += 0.8f;
			thandling += 0.8f;
			ttricks += 0.8f;
			break;
		case Body.Body2:
			tspeed += 1.5f;
			thandling += 0.8f;
			ttricks += 0.8f;
			break;
		case Body.Body3:
			tspeed += 1.2f;
			thandling += 1.2f;
			ttricks += 1.2f;
			break;
		}

		switch(tyre)
		{
		case Tyres.Tyre1:
			tspeed += 0.3f;
			thandling += 0.2f;
			ttricks += 0.2f;
			break;
		case Tyres.Tyre2:
			tspeed += 0.3f;
			thandling += 0.3f;
			ttricks += 0.3f;
			break;
		case Tyres.Tyre3:
			tspeed += 0.2f;
			thandling += 0.3f;
			ttricks += 0.5f;
			break;
		}

		speed = tspeed;
		handling = thandling;
		tricks = ttricks;

		speedText.text = speed.ToString();
		handlingText.text = handling.ToString();
		tricksText.text = tricks.ToString();

		PlayerPrefs.SetFloat("Tricks", tricks);
		PlayerPrefs.SetFloat("Speed", speed);
		PlayerPrefs.SetFloat("Handling", handling);

		bodySelection.text = body.ToString();
		tyreSelection.text = tyre.ToString();
	}

	void mouseInput()
	{
		if ( Input.GetMouseButtonDown(0)){

			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)){
				Debug.Log(hit.transform.gameObject.name);
				if( hit.transform.gameObject.name == "BodyButton")
				{
					NextBody();
				}
				if( hit.transform.gameObject.name == "TyresButton")
				{
					NextTyre();
				}
				if( hit.transform.gameObject.name == "Play")
				{
					Application.LoadLevel("Level1");
				}
			}
		}

	}

}
