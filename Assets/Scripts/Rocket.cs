﻿using UnityEngine;
using System.Collections;

public class Rocket : MonoBehaviour {

	public float movementSpeed;
	public float timeToLive;

	// Use this for initialization
	void Start () {
		// Move it so that it doesnt collide with the ground
		transform.Translate(Vector3.up * 1.5f);

		StartCoroutine(TimedDestroy());
	}
	
	// Update is called once per frame
	void Update () {
		MoveRocket();
	}

	void OnTriggerEnter(Collider collision) {
		//Debug.Log("HIT " + collision.gameObject.tag);
		if (collision.gameObject.tag == "DestructibleObject"){
			DestructableObject ds = collision.gameObject.GetComponent<DestructableObject>();
			if(ds)
				ds.KillObject();


			// Gain fuel for killing an object.
			GameObject.Find("GameManager").GetComponent<FuelManager>().GainFuel(0.3f);
		}

		// Kill the rocket if it collides with anything that isnt the player
		if(collision.gameObject.tag != "PlayerModel"
		   && collision.gameObject.tag != "Player"
		   && collision.gameObject.tag != "Rocket"
		   && collision.gameObject.name != "Ground"
		   && collision.gameObject.tag != "Collectable") 
			DestroyRocket();
	}

	/*
	 * Removes the rocket from the world
	 */
	void DestroyRocket()
	{
		Destroy(gameObject);
	}

	/*
	 * Moves the rocket forward
	 */
	void MoveRocket()
	{
		// Move the player
		transform.Translate(Vector3.forward * movementSpeed * Time.deltaTime);
	}


	/*
	 * Destroys the object after a given amount of time
	 */
	IEnumerator TimedDestroy() {
		yield return new WaitForSeconds(timeToLive);
		DestroyRocket();
	}




}
