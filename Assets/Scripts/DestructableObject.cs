﻿using UnityEngine;
using System.Collections;

/*
 * DestructableObject
 * 
 * Attach this to the DestructableObject game object to give it the
 * appropriate functionality
 */
public class DestructableObject : MonoBehaviour {

	public GameObject deathPrefab;			// The explosion prefab to create when the crate is destroyed


	void Start()
	{
		gameObject.tag = "DestructibleObject";
	}

	/*
	 * This will destroy the object and create the explosion
	 */
	public void KillObject()
	{
		Debug.Log("KillObject called");
		if(deathPrefab)
			Instantiate(deathPrefab, transform.position, transform.rotation);

		Destroy(gameObject);
	}
}
