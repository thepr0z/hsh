﻿using UnityEngine;
using System.Collections;

/*
 * Collectable
 * 
 * Attach this to the collectable game object to give it the
 * appropriate functionality
 */
public class Collectable : MonoBehaviour {

	float rotSpeed = 3.0f;			// How fast the coin will rotate
	int score = 50;					// Amount of score the user will get for getting the collectable
	
	/*
	 * Make the coin rotate
	 */
	void Update () {
		transform.Rotate (0, rotSpeed * Time.deltaTime, 0);
	}

	/*
	 * Checks for collision with the player.
	 * This will add the score, tell the game manager a collectable
	 * has been gathered and destroy the collectable.
	 */
	void OnTriggerEnter(Collider col) {
		if (col.gameObject.tag == "Player") {

			GameObject.Find("GameManager").GetComponent<ScoreManager>().IncreaseScore(score);
			GameObject.Find("GameManager").GetComponent<GameManager>().Collect();
			Destroy(gameObject);
		
		}
	}
}
