﻿using UnityEngine;
using System.Collections;

public class Jump : MonoBehaviour {

	void OnTriggerEnter(Collider collision) {
		if(collision.gameObject.tag == "Player"){

			// Give the pplayer upwards momentum
			float jumpPower = 100;
			collision.rigidbody.AddForce( Vector3.up * jumpPower );

			// Move the player slightly up so that the tricksEnabled check can work properly
			collision.gameObject.transform.Translate(Vector3.up * 0.5f);

			// Enable tricks for the player
			collision.gameObject.GetComponentInChildren<PlayerTricks>().EnableTricks();
		}
		
	}
}
